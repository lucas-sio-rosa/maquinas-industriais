# Importação - Máquinas Industriais

# Decisões de Implementação

As próximas seções explicitam o que foi assumido para o desenvolvimento da solução assim como o que não foi implementado para esta versão.

## O que foi assumido

* Formato dos arquivos - os arquivos tem sempre o mesmo formato, causando um erro caso haja alteração no mesmo;
* Consistência nos tipos de dados - foi assumido os tipos de dados com base no que foi apresentado. Com isso, campos float, integer e boolean foram criados onde pareceram relevantes, sem levar em conta eventuais regras de negócio;
* Nulabilidade - valores NA foram assumidos como nulos;
* Tratamento de dados - valores que não podem ser convertidos são marcados como nulos;
* Logging - não foi montado nenhum sistema de log. Casos como erro de conversão não são informados por exemplo.
* Cargas fixas - um disparo manual do Dataflow deve ser feito para carregar novos arquivos. Isso foi feito para manter os custos baixos uma vez que não há indicação de que os arquivos são gerados ao longo do dia;

## Possibilidades Futuras

* Generalização da arquitetura - tornar a arquitetura mais genérica, importando qualquer arquivo CSV;
* Cargas incrementais - criar um agendamento para importar arquivos novos periodicamente;
* Streaming - se os arquivos chegarem com uma frequencia razoável ao longo do dia, um sistema de streaming poderia prover um modo near-real-time para o BQ;
* Tratamento refinado de dados - se houver muito dado sujo a ser limpo ou muitos dados para apontar inconsistências, poderia-se alterar a modelagem para tabelas de stage (importando tudo como strings) e a criação de estágios intermediários de dados progressivamente mais limpos e tratados (conversão de tipos, nulidade, completude de informações e etc.). Isso também daria logs maiores e mais completos para a solução;
* Testes unitários - criação de testes unitários para as classes de carga, dando mais garantia para o código;

## Decisões de arquitetura

* Utilização do Dataflow para processamento de dados e BigQuery para armazenamento - como soluções gerenciadas da plataforma GCP, tal decisão permite o foco na implementação, uma vez que não é necessário criar e manter uma infraestrutura para que a solução possa ser executada;
* A Importação de dados com tipos convertidos foi feita pensando na apresentação de um relatório básico com os dados, apesar de acarretar na possível perda de informação caso uma inferência de tipo seja feita de forma errônea (já que não há um schema fornecido previamente);

# Projeto - maquinas-industriais

## Getting Started

Este conjunto de programas tem como objetivo importar, via GCP Dataflow, os arquivos em formato csv relativos a operação de uma fábrica de máquinas industriais para tabelas no BigQuery. Cada arquivo .java é responsável por um tipo de arquivo, sendo que todos eles compartilham do mesmo conjunto de parâmetros de configuração:

* [InputFolder] - arquivos a serem processados. O parâmetro espera um wildcard para filtrar os arquivos. Ex: "gs://teste-maquinas-industriais/price_quote*
* [OutputTable] - caminho da tabela BQ onde os registros devem ser inseridos. A tabela não precisa ser criada previamente, mas o dataset deve. A string deve estar no formato legacy SQL. Ex: "stable-journal-181720:MAQUINAS_INDUSTRIAIS.BILL_OF_MATERIALS"
* [TempFolder] - bucket GCS temporário para uso do Dataflow. Ex: "gs://dataflow-staging-lrosa"

### Pre-requirements

Uma conta GCP é necessária para poder rodar o Dataflow e popular as tabelas do BigQuery.
É necessário estar com a váriavel de ambiente do Google configurada com as credenciais do GCP. Ex:
```shell
gcloud auth aplication-default login
echo $GOOGLE_APPLICATION_CREDENTIALS
```

### Installing

O código é instalado utilizando o maven: `mvn clean install`

## Deployment

O código pode ser executado diretamente pelo maven, como no exemplo:

```shell
mvn compile exec:java \
    -Dexec.mainClass=com.main.java.br.ingest.PriceQuotesCsvIngestion \
    -Dexec.args="--project=stable-journal-171820 \
    --stagingLocation=gs://dataflow-staging-lrosa \
    --inputFolder=gs://teste-maquinas-industriais/price_quote* \
    --outputTable=stable-journal-181720:MAQUINAS_INDUSTRIAIS.PRICE_QUOTE \
    --tempFolder=gs://dataflow-staging-lrosa \
    --runner=DataflowRunner \
    --workerMachineType=n1-standard-1 \
    --numWorkers=1 \
    --maxNumWorkers=1"
```

Três scripts shell estão inclusos para facilitar na execução dos dataflows (arquivos execute-*-ingestion.sh).

## Relatório de exemplo

Um relatório de exemplo com base nos dados foi criado no DataStudio e pode ser visualizado através deste [link](https://datastudio.google.com/open/16lyICgwiWe6Z6g8uGGcujc9E8OqnlVUw).

## Built With

* [Java 8](https://docs.oracle.com/javase/8/docs/api/) - Linguagem principal
* [Maven](https://maven.apache.org/) - Gerenciador de dependencias
* [Apache Beam](https://beam.apache.org/) - Motor de Batch e Streaming

### Autores

* [**Lucas Rosa**](https://bitbucket.org:lucas-sio-rosa/)