package com.main.java.br.ingest.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

public class BillOfMaterialRow implements Serializable {

    private String tubeAssemblyId;
    private ArrayList<BillOfMaterialsComponent> componentList;
    private Metadata metadata;
    public static final long serialVersionUID = 1L;

    public BillOfMaterialRow() {
        componentList = new ArrayList<BillOfMaterialsComponent>();
        metadata = new Metadata();

    }

    public static TableSchema getTableSchema() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("TUBE_ASSEMBLY_ID").setType("STRING"));
        fields.add(new TableFieldSchema().setName("COMPONENT_LIST").setType("RECORD")
                .setFields(BillOfMaterialsComponent.getTableSchema()).setMode("REPEATED"));
        fields.add(new TableFieldSchema().setName("METADATA").setType("RECORD")
                .setFields(Metadata.getTableSchema()));
        return new TableSchema().setFields(fields);
    }

    public TableRow castAsTableRow() {
        TableRow row = new TableRow();
        List<TableRow> componentList = new ArrayList<TableRow>();
        row.set("TUBE_ASSEMBLY_ID", this.getTubeAssemblyId());
        for (BillOfMaterialsComponent bomc : this.getComponentList()) {
            componentList.add(bomc.castAsTableRow());
        }
        row.set("COMPONENT_LIST", componentList);
        row.set("METADATA", this.getMetadata().castAsTableRow());
        return row;
    }

    public void processLine(String line) {
        String[] partes = line.split(",");
        this.setTubeAssemblyId(partes[0]);
        String componentName;
        Integer componentQuantity;
        for (int i = 1; i < 17; i += 2) {
            componentName = partes[i].equals("NA") ? null : partes[i];
            try {
                componentQuantity = Integer.parseInt(partes[i + 1]);
            } catch (NumberFormatException e) {
                componentQuantity = null;
            }
            if (componentName != null && componentQuantity != null)
                this.addComponentList(componentName, componentQuantity);
        }
    }

    /**
     * @return the componentList
     */
    public ArrayList<BillOfMaterialsComponent> getComponentList() {
        return componentList;
    }

    /**
     * @param componentList the componentList to set
     */
    public void setComponentList(ArrayList<BillOfMaterialsComponent> componentList) {
        this.componentList = componentList;
    }

    /**
     * @return the metadata
     */
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * @param metadata the metadata to set
     */
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    /**
     * @return the tubeAssemblyId
     */
    public String getTubeAssemblyId() {
        return tubeAssemblyId;
    }

    /**
     * @param tubeAssemblyId the tubeAssemblyId to set
     */
    public void setTubeAssemblyId(String tubeAssemblyId) {
        this.tubeAssemblyId = tubeAssemblyId;
    }

    public void addComponentList(String componentId, Integer componentQuantity) {
        componentList.add(new BillOfMaterialsComponent(componentId, componentQuantity));
    }

    public void setMetadata(String fileName, Long loadTimestamp) {
        metadata.setFileName(fileName);
        metadata.setLoadTimestamp(loadTimestamp);
    }
}