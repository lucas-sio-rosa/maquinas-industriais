package com.main.java.br.ingest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.channels.Channels;

import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.io.FileIO.ReadableFile;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;

import com.main.java.br.ingest.classes.*;

public class ComponentsCsvIngestion {
  public interface CsvIngestionOptions extends PipelineOptions {

    @Description("Path of the file to read from")
    @Default.String("gs://teste-maquinas-industriais/component/*")
    String getInputFolder();

    void setInputFolder(String value);

    @Description("Path of the file to write to")
    @Default.String("stable-journal-181720:MAQUINAS_INDUSTRIAIS.COMPONENTS")
    String getOutputTable();

    void setOutputTable(String value);

    @Description("Dataflow temp location folder")
    @Default.String("gs://dataflow-staging-lrosa")
    String getTempFolder();

    void setTempFolder(String value);
  }

  public static void main(String[] args) {
    CsvIngestionOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CsvIngestionOptions.class);
    Pipeline p = Pipeline.create(options);

    options.setTempLocation(options.getTempFolder());

    TableSchema schema = ComponentRow.getTableSchema();

    p.apply("Get Files", FileIO.match().filepattern(options.getInputFolder())).apply("Read Lines", FileIO.readMatches())
        .apply("Parse File", ParDo.of(new DoFn<ReadableFile, ComponentRow>() {
          @ProcessElement
          public void processElement(ProcessContext c) throws Exception {
            ReadableFile f = c.element();

            String line;
            try (BufferedReader r = new BufferedReader(new InputStreamReader(Channels.newInputStream(f.open())))) {
              while ((line = r.readLine()) != null) {
                ComponentRow obj = new ComponentRow();
                obj.processLine(line);
                obj.setMetadata(f.getMetadata().resourceId().toString(), System.currentTimeMillis() / 1000);
                c.output(obj);
              }
            }

          }
        })).apply("Remove Header", ParDo.of(new DoFn<ComponentRow, ComponentRow>() {
          @ProcessElement
          public void processElement(ProcessContext c) throws Exception {
            if (!c.element().getComponentId().equals("component_id")) {
              c.output(c.element());
            }
          }
        })).apply("Convert to BQ", ParDo.of(new DoFn<ComponentRow, TableRow>() {
          @ProcessElement
          public void processElement(ProcessContext c) throws Exception {
            c.output(c.element().castAsTableRow());
          }
        })) //
        .apply("Write to BQ", BigQueryIO.writeTableRows().to(options.getOutputTable())//
            .withSchema(schema)//
            .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
            .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED));

    p.run();
  }
}
