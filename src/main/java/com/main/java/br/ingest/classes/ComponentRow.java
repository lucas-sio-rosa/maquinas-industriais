package com.main.java.br.ingest.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

/**
 * Component
 */
public class ComponentRow implements Serializable {
    public static final long serialVersionUID = 1L;
    private String componentId;
    private String componentTypeId;
    private String type;
    private String connectionTypeId;
    private String outsideShape;
    private String baseType;
    private Float heightOverTube;
    private Float boltPatternLong;
    private Float boltPatternWide;
    private Boolean groove;
    private Float baseDiameter;
    private Float shoulderDiameter;
    private Boolean uniqueFeature;
    private Boolean orientation;
    private Float weight;
    private Metadata metadata;

    public ComponentRow() {
        this.metadata = new Metadata();
    }

    public void setMetadata(String fileName, Long loadTimestamp) {
        metadata.setFileName(fileName);
        metadata.setLoadTimestamp(loadTimestamp);
    }

    public static TableSchema getTableSchema() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("COMPONENT_ID").setType("STRING"));
        fields.add(new TableFieldSchema().setName("COMPONENT_TYPE_ID").setType("STRING"));
        fields.add(new TableFieldSchema().setName("TYPE").setType("STRING"));
        fields.add(new TableFieldSchema().setName("CONNECTION_TYPE_ID").setType("STRING"));
        fields.add(new TableFieldSchema().setName("OUTSIDE_SHAPE").setType("STRING"));
        fields.add(new TableFieldSchema().setName("BASE_TYPE").setType("STRING"));
        fields.add(new TableFieldSchema().setName("HEIGHT_OVER_TUBE").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("BOLT_PATTERN_LONG").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("BOLT_PATTERN_WIDE").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("GROOVE").setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName("BASE_DIAMETER").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("SHOULDER_DIAMETER").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("UNIQUE_FEATURE").setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName("ORIENTATION").setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName("WEIGHT").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("METADATA").setType("RECORD").setFields(Metadata.getTableSchema()));

        return new TableSchema().setFields(fields);
    }

    public TableRow castAsTableRow() {
        TableRow row = new TableRow();
        row.set("COMPONENT_ID", this.getComponentId());
        row.set("COMPONENT_TYPE_ID", this.getComponentTypeId());
        if(this.getType() != null)
            row.set("TYPE", this.getType());
        if(this.getConnectionTypeId() != null)
            row.set("CONNECTION_TYPE_ID", this.getConnectionTypeId());
        if(this.getOutsideShape() != null)
            row.set("OUTSIDE_SHAPE", this.getOutsideShape());
        if(this.getBaseType() != null)
            row.set("BASE_TYPE", this.getBaseType());
        if(this.getHeightOverTube() != null)
            row.set("HEIGHT_OVER_TUBE", this.getHeightOverTube());
        if(this.getBoltPatternLong() != null)
            row.set("BOLT_PATTERN_LONG", this.getBoltPatternLong());
        if(this.getBoltPatternWide() != null)
            row.set("BOLT_PATTERN_WIDE", this.getBoltPatternWide());
        if(this.getGroove() != null)
            row.set("GROOVE", this.getGroove());
        if(this.getBaseDiameter() != null)
            row.set("BASE_DIAMETER", this.getBaseDiameter());
        if(this.getShoulderDiameter() != null)
            row.set("SHOULDER_DIAMETER", this.getShoulderDiameter());
        if(this.getUniqueFeature() != null)
            row.set("UNIQUE_FEATURE", this.getUniqueFeature());
        if(this.getOrientation() != null)
            row.set("ORIENTATION", this.getOrientation());
        if(this.getWeight() != null)
            row.set("WEIGHT", this.getWeight());
        row.set("METADATA", this.getMetadata().castAsTableRow());
        return row;
    }

    private Boolean customParseBoolean(String s) {
        if(s.equals("Yes")) {
            return true;
        } else {
            if(s.equals("No")) {
                return false;
            } else {
                return null;
            }
        }
    }

    public void processLine(String line) {
        String[] partes = line.split(",");
        this.setComponentId(partes[0]);
        this.setComponentTypeId(partes[1]);
        this.setType(partes[2].equals("NA") ? null : partes[2]);
        this.setConnectionTypeId(partes[3].equals("NA") ? null : partes[3]);
        this.setOutsideShape(partes[4].equals("NA") ? null : partes[4]);
        this.setBaseType(partes[5].equals("NA") ? null : partes[5]);
        try {
            this.setHeightOverTube(Float.parseFloat(partes[6]));
        } catch(NumberFormatException e) {
            this.setHeightOverTube(null);
        }
        try {
            this.setBoltPatternLong(Float.parseFloat(partes[7]));
        } catch(NumberFormatException e) {
            this.setBoltPatternLong(null);
        }
        try {
            this.setBoltPatternWide(Float.parseFloat(partes[8]));
        } catch(NumberFormatException e) {
            this.setBoltPatternWide(null);
        }
        this.setGroove(customParseBoolean(partes[9]));
        
        try {
            this.setBaseDiameter(Float.parseFloat(partes[10]));
        } catch(NumberFormatException e) {
            this.setBaseDiameter(null);
        }
        try {
            this.setShoulderDiameter(Float.parseFloat(partes[11]));
        } catch(NumberFormatException e) {
            this.setBaseDiameter(null);
        }
        this.setUniqueFeature(customParseBoolean(partes[12]));
        this.setOrientation(customParseBoolean(partes[13]));
        try {
            this.setWeight(Float.parseFloat(partes[14]));
        } catch(NumberFormatException e) {
            this.setBaseDiameter(null);
        }
    }
    /**
     * @param baseDiameter the baseDiameter to set
     */
    public void setBaseDiameter(Float baseDiameter) {
        this.baseDiameter = baseDiameter;
    }

    /**
     * @return the baseDiameter
     */
    public Float getBaseDiameter() {
        return baseDiameter;
    }
    
    /**
     * @param baseType the baseType to set
     */
    public void setBaseType(String baseType) {
        this.baseType = baseType;
    }

    /**
     * @return the baseType
     */
    public String getBaseType() {
        return baseType;
    }

    /**
     * @param boltPatternLong the boltPatternLong to set
     */
    public void setBoltPatternLong(Float boltPatternLong) {
        this.boltPatternLong = boltPatternLong;
    }

    /**
     * @return the boltPatternLong
     */
    public Float getBoltPatternLong() {
        return boltPatternLong;
    }

    /**
     * @param boltPatternWide the boltPatternWide to set
     */
    public void setBoltPatternWide(Float boltPatternWide) {
        this.boltPatternWide = boltPatternWide;
    }

    /**
     * @return the boltPatternWide
     */
    public Float getBoltPatternWide() {
        return boltPatternWide;
    }

    /**
     * @param componentId the componentId to set
     */
    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    /**
     * @return the componentId
     */
    public String getComponentId() {
        return componentId;
    }

    /**
     * @param componentTypeId the componentTypeId to set
     */
    public void setComponentTypeId(String componentTypeId) {
        this.componentTypeId = componentTypeId;
    }

    /**
     * @return the componentTypeId
     */
    public String getComponentTypeId() {
        return componentTypeId;
    }

    /**
     * @param connectionTypeId the connectionTypeId to set
     */
    public void setConnectionTypeId(String connectionTypeId) {
        this.connectionTypeId = connectionTypeId;
    }

    /**
     * @return the connectionTypeId
     */
    public String getConnectionTypeId() {
        return connectionTypeId;
    }

    /**
     * @param groove the groove to set
     */
    public void setGroove(Boolean groove) {
        this.groove = groove;
    }

    /**
     * @return the groove
     */
    public Boolean getGroove() {
        return groove;
    }

    /**
     * @param heightOverTube the heightOverTube to set
     */
    public void setHeightOverTube(Float heightOverTube) {
        this.heightOverTube = heightOverTube;
    }

    /**
     * @return the heightOverTube
     */
    public Float getHeightOverTube() {
        return heightOverTube;
    }

    /**
     * @param metadata the metadata to set
     */
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    /**
     * @return the metadata
     */
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * @param orientation the orientation to set
     */
    public void setOrientation(Boolean orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the orientation
     */
    public Boolean getOrientation() {
        return orientation;
    }

    /**
     * @param outsideShape the outsideShape to set
     */
    public void setOutsideShape(String outsideShape) {
        this.outsideShape = outsideShape;
    }

    /**
     * @return the outsideShape
     */
    public String getOutsideShape() {
        return outsideShape;
    }

    /**
     * @param shoulderDiameter the shoulderDiameter to set
     */
    public void setShoulderDiameter(Float shoulderDiameter) {
        this.shoulderDiameter = shoulderDiameter;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the shoulderDiameter
     */
    public Float getShoulderDiameter() {
        return shoulderDiameter;
    }

    /**
     * @param uniqueFeature the uniqueFeature to set
     */
    public void setUniqueFeature(Boolean uniqueFeature) {
        this.uniqueFeature = uniqueFeature;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Float weight) {
        this.weight = weight;
    }

    /**
     * @return the uniqueFeature
     */
    public Boolean getUniqueFeature() {
        return uniqueFeature;
    }

    /**
     * @return the weight
     */
    public Float getWeight() {
        return weight;
    }
}