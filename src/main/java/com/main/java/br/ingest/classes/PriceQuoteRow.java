package com.main.java.br.ingest.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

import org.joda.time.DateTime;

/**
 * Component
 */
public class PriceQuoteRow implements Serializable {
    public static final long serialVersionUID = 1L;
    private String tubeAssemblyId;
    private String supplier;
    private DateTime quoteDate;
    private Integer annualUsage;
    private Integer minOrderQuantity;
    private Boolean bracketPricing;
    private Integer quantity;
    private Float cost;
    private Metadata metadata;

    public PriceQuoteRow() {
        this.metadata = new Metadata();
    }

    public void setMetadata(String fileName, Long loadTimestamp) {
        metadata.setFileName(fileName);
        metadata.setLoadTimestamp(loadTimestamp);
    }

    public static TableSchema getTableSchema() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("TUBE_ASSEMBLY_ID").setType("STRING"));
        fields.add(new TableFieldSchema().setName("SUPPLIER").setType("STRING"));
        fields.add(new TableFieldSchema().setName("QUOTE_DATE").setType("DATETIME"));
        fields.add(new TableFieldSchema().setName("ANNUAL_USAGE").setType("INT64"));
        fields.add(new TableFieldSchema().setName("MIN_ORDER_QUANTITY").setType("INT64"));
        fields.add(new TableFieldSchema().setName("BRACKET_PRICING").setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName("QUANTITY").setType("INT64"));
        fields.add(new TableFieldSchema().setName("COST").setType("FLOAT64"));
        fields.add(new TableFieldSchema().setName("METADATA").setType("RECORD").setFields(Metadata.getTableSchema()));

        return new TableSchema().setFields(fields);
    }

    public TableRow castAsTableRow() {
        TableRow row = new TableRow();
        row.set("TUBE_ASSEMBLY_ID", this.getTubeAssemblyId());
        row.set("SUPPLIER", this.getSupplier());
        row.set("QUOTE_DATE", this.getQuoteDate().toString("yyyy-MM-dd"));
        if(this.getAnnualUsage() != null)
            row.set("ANNUAL_USAGE", this.getAnnualUsage());
        if(this.getMinOrderQuantity() != null)
            row.set("MIN_ORDER_QUANTITY", this.getMinOrderQuantity());
        if(this.getBracketPricing() != null)
            row.set("BRACKET_PRICING", this.getBracketPricing());
        if(this.getQuantity() != null)
            row.set("QUANTITY", this.getQuantity());
        if(this.getCost() != null)
            row.set("COST", this.getCost());
        row.set("METADATA", this.getMetadata().castAsTableRow());
        return row;
    }

    private Boolean customParseBoolean(String s) {
        if(s.equals("Yes")) {
            return true;
        } else {
            if(s.equals("No")) {
                return false;
            } else {
                return null;
            }
        }
    }

    public void processLine(String line) {
        String[] partes = line.split(",");
        this.setTubeAssemblyId(partes[0]);
        this.setSupplier(partes[1]);
        try {
            this.setQuoteDate(DateTime.parse(partes[2]));
        } catch(Exception e) {
            this.setQuoteDate(null);
        }
        try {
            this.setAnnualUsage(Integer.parseInt(partes[3]));
        } catch(NumberFormatException e) {
            this.setAnnualUsage(null);
        }
        try {
            this.setMinOrderQuantity(Integer.parseInt(partes[4]));
        } catch (NumberFormatException e) {
            this.setMinOrderQuantity(null);
        }
        this.setBracketPricing(customParseBoolean(partes[5]));
        try {
            this.setQuantity(Integer.parseInt(partes[6]));
        } catch (NumberFormatException e) {
            this.setQuantity(null);
        }
        try {
            this.setCost(Float.parseFloat(partes[7]));
        } catch (NumberFormatException e) {
            this.setCost(null);
        }
    }
    
    /**
     * @param annualUsage the annualUsage to set
     */
    public void setAnnualUsage(Integer annualUsage) {
        this.annualUsage = annualUsage;
    }

    /**
     * @return the annualUsage
     */
    public Integer getAnnualUsage() {
        return annualUsage;
    }

    /**
     * @param bracketPricing the bracketPricing to set
     */
    public void setBracketPricing(Boolean bracketPricing) {
        this.bracketPricing = bracketPricing;
    }

    /**
     * @return the bracketPricing
     */
    public Boolean getBracketPricing() {
        return bracketPricing;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(Float cost) {
        this.cost = cost;
    }

    /**
     * @return the cost
     */
    public Float getCost() {
        return cost;
    }

    /**
     * @param minOrderQuantity the minOrderQuantity to set
     */
    public void setMinOrderQuantity(Integer minOrderQuantity) {
        this.minOrderQuantity = minOrderQuantity;
    }

    /**
     * @return the metadata
     */
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the minOrderQuantity
     */
    public Integer getMinOrderQuantity() {
        return minOrderQuantity;
    }

    /**
     * @param quoteDate the quoteDate to set
     */
    public void setQuoteDate(DateTime quoteDate) {
        this.quoteDate = quoteDate;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param supplier the supplier to set
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * @return the quoteDate
     */
    public DateTime getQuoteDate() {
        return quoteDate;
    }

    /**
     * @param tubeAssemblyId the tubeAssemblyId to set
     */
    public void setTubeAssemblyId(String tubeAssemblyId) {
        this.tubeAssemblyId = tubeAssemblyId;
    }

    /**
     * @return the supplier
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * @return the tubeAssemblyId
     */
    public String getTubeAssemblyId() {
        return tubeAssemblyId;
    }

}