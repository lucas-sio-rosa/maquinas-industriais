package com.main.java.br.ingest.classes;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;

/**
 * BillOfMaterialsComponent
 */
public class BillOfMaterialsComponent implements Serializable {
    private String componentName;
    private Integer componentQuantity;
    public static final long serialVersionUID = 1L;

    public BillOfMaterialsComponent() {
        super();
    }

    public BillOfMaterialsComponent(String componentName, Integer componentQuantity) {
        this.componentName = componentName;
        this.componentQuantity = componentQuantity;
    }

    public TableRow castAsTableRow() {
        TableRow row = new TableRow();
        row = new TableRow().set("COMPONENT_ID", this.getComponentName()).set("COMPONENT_QUANTITY",
                this.getComponentQuantity());
        return row;
    }

    public static ArrayList<TableFieldSchema> getTableSchema() {
        return new ArrayList<TableFieldSchema>() {
            {
                add(new TableFieldSchema().setName("COMPONENT_ID").setType("STRING"));
                add(new TableFieldSchema().setName("COMPONENT_QUANTITY").setType("INTEGER"));
            }
        };
    }

    /**
     * @return the componentName
     */
    public String getComponentName() {
        return componentName;
    }

    /**
     * @return the componentQuantity
     */
    public Integer getComponentQuantity() {
        return componentQuantity;
    }

    /**
     * @param componentName the componentName to set
     */
    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    /**
     * @param componentQuantity the componentQuantity to set
     */
    public void setComponentQuantity(Integer componentQuantity) {
        this.componentQuantity = componentQuantity;
    }
}