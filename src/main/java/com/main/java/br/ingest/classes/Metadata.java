package com.main.java.br.ingest.classes;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;

public class Metadata implements Serializable {
  private String fileName;
  private Long loadTimestamp;
  private static final long serialVersionUID = 1L;

  public Metadata() {
    super();
  }

  public void setMetadata(String fileName, Long loadTimestamp) {
    this.fileName = fileName;
    this.loadTimestamp = loadTimestamp;
  }

  public TableRow castAsTableRow() {
    return new TableRow().set("FILENAME", this.getFileName()).set("LOAD_TIMESTAMP", this.getLoadTimestamp());
  }

  public static ArrayList<TableFieldSchema> getTableSchema() {
    return new ArrayList<TableFieldSchema>() {
      {
        add(new TableFieldSchema().setName("FILENAME").setType("STRING"));
        add(new TableFieldSchema().setName("LOAD_TIMESTAMP").setType("TIMESTAMP"));
      }
    };
  }

  /**
   * @return the fileName
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * @param fileName the fileName to set
   */
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   * @return the loadTimestamp
   */
  public Long getLoadTimestamp() {
    return loadTimestamp;
  }

  /**
   * @param loadTimestamp the loadTimestamp to set
   */
  public void setLoadTimestamp(Long loadTimestamp) {
    this.loadTimestamp = loadTimestamp;
  }
}
